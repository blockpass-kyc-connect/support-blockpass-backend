const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const multer = require('multer');
var mongoose = require('mongoose');
const ServerSDK = require('bp-sdk-server');
const { KYCModel, FileStorage } = require('./SimpleStorage');
var KycModel = require('./models/KycModel');

const config = {
  BASE_URL: 'https://sandbox-api.blockpass.org',
  BLOCKPASS_CLIENT_ID: 'gosecurity_service',
  BLOCKPASS_SECRET_ID: 'ab7f0f0a983a4effdeb8a16c413775f7'
};

mongoose.connect('mongodb://localhost:27017/blockpass');

// -------------------------------------------------------------------------
//  Logic Handler
// -------------------------------------------------------------------------
async function findKycById(kycId) {
  return await KYCModel.findOne({ blockPassID: kycId })
}

async function createKyc({ kycProfile, refId }) {
  const { id, smartContractId, rootHash, isSynching } = kycProfile;
  const newIns = new KYCModel({
    blockPassID: id,
    refId,
    rootHash,
    smartContractId,
    isSynching
  });

  newIns.certs = serverSdk.certs.reduce((acc, key) => {
    acc[key] = {
      slug: key,
      status: 'missing'
    };
    return acc
  }, {});

  newIns.identities = serverSdk.requiredFields.reduce((acc, key) => {
    acc[key] = {
      slug: key,
      status: 'missing'
    };
    return acc
  }, {});

  return await newIns.save()
}

async function updateKyc({ kycRecord, kycProfile, kycToken, userRawData }) {
  const { id, smartContractId, rootHash, isSynching } = kycProfile;

  const jobs = Object.keys(userRawData).map(async key => {
    const metaData = userRawData[key];

    if (metaData.type === 'string') {
      if (metaData.isCert) {
        return (kycRecord.certs[key] = {
          slug: key,
          value: metaData.value,
          status: 'received',
          comment: ''
        })
      } else {
        return (kycRecord.identities[key] = {
          slug: key,
          value: metaData.value,
          status: 'received',
          comment: ''
        })
      }
    }

    const { buffer, originalname } = metaData;
    const ext = originalname.split('.')[1];
    const fileName = `${id}_${key}.${ext}`;
    const fileHandler = await FileStorage.writeFile({
      fileName,
      mimetype: `image/${ext}`,
      fileBuffer: buffer
    });

    return (kycRecord.identities[key] = {
      slug: key,
      value: fileHandler._id,
      isFile: true,
      status: 'received',
      comment: ''
    })
  });

  await Promise.all(jobs);

  // calculate token expired date from 'expires_in'
  const expiredDate = new Date(Date.now() + kycToken.expires_in * 1000);
  kycRecord.bpToken = {
    ...kycToken,
    expires_at: expiredDate
  };

  kycRecord.status = 'inreview';
  kycRecord.rootHash = rootHash;
  kycRecord.smartContractId = smartContractId;
  kycRecord.isSynching = isSynching;

  return await kycRecord.save()
}

async function generateSsoPayload({
  kycProfile,
  kycRecord,
  kycToken,
  payload
}) {
  return {
    _id: kycRecord._id
  }
}

async function queryKycStatus({ kycRecord }) {
  const { status, identities, certs } = kycRecord;

  // status meaning:
  //  - "received": data recieved by service and under review
  //  - "approved": data fields approved by service
  //  - "rejected": data rejected by service. Please provide comments for this
  //     => Mobile app will asking user to update
  //  - "missing": data fields missing - Uploading error
  //     => Mobile app will asking user to re-upload
  const identitiesStatus = Object.keys(identities).map(key => {
    const itm = identities[key];
    const { slug, status, comment } = itm;
    return {
      slug,
      status,
      comment
    }
  });

  const certsStatus = Object.keys(certs).map(key => {
    const itm = certs[key];
    const { slug, status } = itm;
    return {
      slug,
      status
    }
  });

  return {
    status,
    message: 'This process usually take 2 working days',
    createdDate: new Date(),
    identities: identitiesStatus,
    certificates: certsStatus,
    allowResubmit: true
  }
}

// -------------------------------------------------------------------------
// Express app
// -------------------------------------------------------------------------
const app = express();
const upload = multer();
const fs = require('fs');
const util = require('util');

// Allow access origin
app.use(cors({}));
app.disable('x-powered-by');

// middleware
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const router = express.Router();
app.use(router);

router.get('/', (req, res) => {
    //fs.writeFileSync('debug.txt', 'test row');
    //fs.writeFileSync('debug.json', util.inspect(req) , 'utf-8');
    res.json('Get: hello');
});

router.post('/', (req, res) => {
    res.json('Post: hello');
});

// -------------------------------------------------------------------------
// Api
// -------------------------------------------------------------------------
router.post('/blockpass/api/uploadData', upload.any(), async (req, res) => {

  fs.writeFileSync('debug.txt', 'action: uploadData');

  try {
    const { accessToken, slugList, ...userRawFields } = req.body;
    const files = req.files || [];

    // Flattern user data
    const userRawData = {};

    Object.keys(userRawFields).forEach(key => {
      const originalKey = key;
      const isCert = key.startsWith('[cer]');
      if (isCert) key = key.slice('[cer]'.length);

      userRawData[key] = {
        type: 'string',
        value: userRawFields[originalKey],
        isCert
      }
    });

    files.forEach(itm => {
      userRawData[itm.fieldname] = {
        type: 'file',
        ...itm
      }
    });

    const payload = await serverSdk.updateDataFlow({
      accessToken,
      slugList,
      ...userRawData
    });
    return res.json(payload)
  } catch (ex) {
    console.error(ex);
    return res.status(500).json({
      err: 500,
      msg: ex.message
    })
  }
});

// -------------------------------------------------------------------------
router.post('/blockpass/api/login', async (req, res) => {

  fs.writeFileSync('debug.txt', 'action: login');

  try {
    const { code, sessionCode, refId } = req.body;
      fs.appendFile('debug.txt', '\nreq.body: ' + util.inspect(req.body), 'utf-8');
      fs.appendFile('debug.txt', '\nobj: ' + util.inspect({ code, sessionCode }), 'utf-8');

      const payload = await serverSdk.loginFow({ code, sessionCode, refId });
    return res.json(payload)
  } catch (ex) {
    console.error(ex);
    return res.status(500).json({
      err: 500,
      msg: ex.message
    })
  }
});

// -------------------------------------------------------------------------
router.post('/blockpass/api/register', async (req, res) => {

  fs.writeFileSync('debug.txt', 'action: register');

  try {
    const { code, refId } = req.body;

    const payload = await serverSdk.registerFlow({ code, refId });
    return res.json(payload)
  } catch (ex) {
    console.error(ex);
    return res.status(500).json({
      err: 500,
      msg: ex.message
    })
  }
});

// -------------------------------------------------------------------------
router.post('/blockpass/api/resubmit', async (req, res) => {

  fs.writeFileSync('debug.txt', 'action: resubmit');

  try {
    const { code, fieldList, certList } = req.body;

    const payload = await serverSdk.resubmitDataFlow({
      code,
      fieldList,
      certList
    });
    return res.json(payload)
  } catch (ex) {
    console.error(ex);
    return res.status(500).json({
      err: 500,
      msg: ex.message
    })
  }
});

// -------------------------------------------------------------------------
router.post('/blockpass/api/status', async (req, res) => {

  fs.writeFileSync('debug.txt', 'action: status');

  try {
    const { code, sessionCode } = req.body;
      fs.appendFile('debug.txt', '\nreq.body: ' + util.inspect(req.body), 'utf-8');
      fs.appendFile('debug.txt', '\nobj: ' + util.inspect({ code, sessionCode }), 'utf-8');

    const payload = await serverSdk.queryStatusFlow({ code, sessionCode });
    return res.json(payload)
  } catch (ex) {
    console.error(ex);
    return res.status(500).json({
      err: 500,
      msg: ex.message
    })
  }
});

// -------------------------------------------------------------------------
router.post('/util/sendPn', async (req, res) => {

  fs.writeFileSync('debug.txt', 'action: sendPn');

  try {
    const { bpId, title = 'title', message = 'body' } = req.body;

    if (!bpId) {
      return res.status(400).json({
        err: 400,
        msg: 'Missing blockpass Id'
      })
    }

    const kycRecord = await KYCModel.findOne({ blockPassID: bpId });

    if (!kycRecord) {
      return res.status(404).json({
        err: 404,
        msg: 'kycRecord not found'
      })
    }

    const response = await serverSdk.userNotify({
      title,
      message,
      bpToken: kycRecord.bpToken
    });

    const { bpToken } = response;

    // update refreshed Token
    if (bpToken !== kycRecord.bpToken) {
      kycRecord.bpToken = bpToken;
      await kycRecord.save()
    }

    return res.json(response.res)
  } catch (ex) {
    console.error(ex);
    return res.status(500).json({
      err: 500,
      msg: ex.message
    })
  }
});




/**
 * KycModel
 */
// Add new kyc model
router.post('/blockpass/api/setkyc', function (req, res) {

    fs.writeFileSync('debug.json', util.inspect(req.body) , 'utf-8');

    var kycModelObj = new KycModel({
        blockPassID: req.body.blockPassID,
        refId: 'refId',
        rootHash: 'rootHash',
        identities: [
            {
                slug: 'id_key1_slug',
                status: 'id_key1_status'
            },
            {
                slug: 'id_key2_slug',
                status: 'id_key2_status'
            },
        ],
        certs: [
            {
                slug: 'cert_key1_slug',
                status: 'cert_key1_status'
            }
        ]
    });

    // save the sample entry
    kycModelObj.save(function(err) {
        if (err) throw err;

        console.log('kycModel saved successfully');
        res.json({ success: true });
    });

});

// Get kyc model by ID
router.post('/blockpass/api/getkyc', function(req, res) {

    fs.writeFileSync('debug.json', util.inspect(req.body) , 'utf-8');

    KycModel.findOne({
        blockPassID: req.body.blockPassID
    }, function(err, kycModel) {

        if (err) throw err;

        if (!kycModel) {

            res.json({ success: false, message: 'Model not found.' });

        } else {

            res.json({
                success: true,
                message: 'Enjoy your kycModel!',
                model: kycModel
            });

        }

    });

});


// -------------------------------------------------------------------------
//  Blockpass Server SDK
// -------------------------------------------------------------------------
const serverSdk = new ServerSDK({
  baseUrl: config.BASE_URL,
  clientId: config.BLOCKPASS_CLIENT_ID,
  secretId: config.BLOCKPASS_SECRET_ID,
  autoFetchMetadata: true,

  // Custom implement
  findKycById: findKycById,
  createKyc: createKyc,
  updateKyc: updateKyc,
  queryKycStatus: queryKycStatus,
  generateSsoPayload: generateSsoPayload
});

// Sdk loaded
serverSdk.once('onLoaded', _ => {
  const port = process.env.SERVER_PORT || 3344;
  let server = app.listen(port, '0.0.0.0', function() {
    console.log(`Listening on port ${port}...`)
  });

  // gracefull shutdown
  app.close = _ => server.close()
});

// Sdk error
serverSdk.once('onError', err => {
  console.error(err);
  process.exit(1)
});

module.exports = app;
