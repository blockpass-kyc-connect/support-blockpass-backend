// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
var model = mongoose.model('KycModel', new Schema({
    blockPassID: String,
    refId: String,
    rootHash: String,
    identities: [
        {slug: String, status: String},
        {slug: String, status: String},
    ],
    certs: [
        {slug: String, status: String},
    ]
}));

module.exports = model;
